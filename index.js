/***
API reference guide:
https://www.cisco.com/c/dam/en/us/td/docs/conferencing/ciscoMeetingServer/Reference_Guides/Version-2-2/Cisco-Meeting-Server-API-Reference-Guide-2-2.pdf
***/
const Spaces = require('./spaces')

module.exports = class CmsClient {
  constructor({ url, username, password }) {
    if (!url || url === '') throw 'url parameter must be provided'
    if (!username || username === '') throw 'username parameter must be provided'
    if (!password || password === '') throw 'password parameter must be provided'
    // this.url = url
    // this.username = username
    // this.password = password
    this.spaces = new Spaces({ url, username, password })
  }
}
