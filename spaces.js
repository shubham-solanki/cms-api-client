const axios = require('axios')
const xml2js = require('xml2js-es6-promise')
const querystring = require('querystring')

async function processError (e) {
  // xml parsing options
  const opts = {
    explicitArray: false,
    trim: true
  }
  try {
    // console.log('area2')
    // console.log(e.response.data)
    const error = await xml2js(e.response.data, opts)
    return error
  } catch (e2) {
    // console.log('area3')
    // console.log('e2', e2)
    return e
  }
}

// if input is array, returns input; else, return input object inside an array
function makeArray (obj) {
  if (!Array.isArray(obj)) {
    return [obj]
  } else {
    return obj
  }
}

function formatSpaces (spaces) {
  // format list
  spaces.forEach(e => {
    // write id to element.id property
    e.id = e.$.id
    // and remove that $ property
    delete e.$
  })
  // return the object even though it is modified in-place
  return spaces
}

module.exports = class Spaces {
  constructor({ url, username, password }) {
    this.url = `${url}/coSpaces`
    this.auth = {
      username,
      password
    }
  }

  async list(params) {
    let response
    try {
      response = await axios.get(this.url, {params, auth: this.auth})
    } catch (e) {
      throw e
    }

    // parse xml response to json
    try {
      // xml parsing options
      const opts = {
        explicitArray: false,
        trim: true
      }
      // parse xml to json and return that json data as an array
      return await xml2js(response.data, opts)
    } catch (e) {
      console.log('error parsing XML to JSON:', e)
      throw e
    }
  }

  async listAll() {
    // our return array
    let ret = []
    // CMS API max supported limit is 19
    let limit = 19
    // start at the beginning
    let offset = 0
    try {
      // get first set of spaces, up to 19
      const response1 = await this.list({limit, offset})
      // remember the total
      const total = response1.coSpaces.$.total
      // put spaces into return array
      makeArray(response1.coSpaces.coSpace).forEach(v => {
        ret.push(v)
      })
      // did we get them all?
      while (total > limit + offset) {
        // increase offset
        offset += limit
        // get next set of spaces
        const response2 = await this.list({limit, offset})
        // add results to the return array
        // put spaces into return array
        makeArray(response2.coSpaces.coSpace).forEach(v => {
          ret.push(v)
        })
      }
      formatSpaces(ret)
      // return entire formatted list
      return ret
    } catch (e) {
      throw e
    }
  }

  async create (params) {
    let data = querystring.stringify(params)
    try {
      return await axios.post(this.url, data, {auth: this.auth})
    } catch (e) {
      throw processError(e)
    }
  }

  async delete (id) {
    try {
      return await axios.delete(`${this.url}/${id}`, {auth: this.auth})
    } catch (e) {
      throw processError(e)
    }
  }

  async resetPasscode ({uri, passcode}) {
    const space = await this.find(uri)
    const data = querystring.stringify({passcode})
    try {
      return await axios.put(`${this.url}/${space.id}`, data, {auth: this.auth})
    } catch (e) {
      throw processError(e)
    }
  }

  // retrieve the Space from uriUserPart
  async find (uri) {
    // ask the server to filter results to the uri provided
    const rsp = await this.list({
      filter: uri
    })
    // console.log('search response: ', rsp)
    const total = rsp.coSpaces.$.total
    if (total === '0' || total === 0) {
      throw `Space with uri '${uri}' not found`
    }
    // make sure spaces is an Array
    const spaces = makeArray(rsp.coSpaces.coSpace)
    formatSpaces(spaces)
    // console.log('formatted spaces: ', spaces)
    // try to find the exact match
    const match = spaces.find(v => {
      return v.uri === uri
    })
    // console.log(match)
    // if we found an exact match return the ID
    if (match) {
      return match
    } else {
      throw 'no exact match found'
    }
  }
}
