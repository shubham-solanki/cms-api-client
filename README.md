# cms-api-client
This is a JavaScript client for the Cisco Meeting Server API (currently built against v2.2 of the CMS API).

# Functions
Currently this library can create a Space, list Spaces with pagination, and list all Spaces.

# Usage
```js
const CmsClient = require('cms-api-client')
let client = new CmsClient({
  url: 'https://cms.mydomain.com:445/api/v1',
  username: 'api',
  password: 'pa$$w0rd'
})

client.spaces.create({
  name: 'Jane Smith',
  email: 'jsmith@mydomain.com',
  uri: 'jsmith',
  secondaryUri: '5551112222',
  callId: '5551112222',
  passcode: '123456'
})
.then(response => {
  console.log(response)
})
.catch(error => {
  console.log(error)
})

client.spaces.listAll()
.then(response => {
  console.log(response)
})
.catch(error => {
  console.log(error)
})
```
