// generate random digit
function getRandom() {
  var possible = "0123456789"
  return possible.charAt(Math.floor(Math.random() * possible.length))
}

// CMS server connection info
let clientConfig = {
  url: 'https://cms.mydomain.com:445/api/v1',
  username: 'api',
  password: 'pa$$w0rd'
}

// parameters to create a space during Mocha tests
let createSpaceParams = {
  name: 'Mocha Test',
  email: 'mochatest@mydomain.com',
  uri: 'mochatest',
  secondaryUri: '5551112222',
  callId: '5551112222',
  passcode: '123456'
}

// generate a random string of digits
let random = ''
for (let i = 0; i < 6; i++) {
  random += getRandom()
}

// add random string of digits to params that should be unique
createSpaceParams.uri = createSpaceParams.uri + random
createSpaceParams.callId = createSpaceParams.callId + random
createSpaceParams.secondaryUri = createSpaceParams.secondaryUri + random

module.exports ={
  clientConfig,
  createSpaceParams
}
