'use strict'
const CmsClient = require('../index')
const config = require('./test.config.js')

describe(`Test Cisco Meeting Server API operations`, () => {
  // init client
  const client = new CmsClient(config.clientConfig)
  // for storing the uuid of the Space we create and then delete durng tests
  let uuid
  // test spaces.listAll
  it(`should retrieve list of all existing spaces on CMS server`, function(done) {
    client.spaces.listAll()
    .then(response => {
      done()
    })
    .catch(error => {
      done(error)
    })
  })

  // test spaces.create
  it('should create a space, if this test has not created the same space before', function(done) {
    // console.log(config.createSpaceParams)
    client.spaces.create(config.createSpaceParams)
    .then(response => {
      done()
    })
    .catch(error => {
      done(error)
    })
  })

  // test spaces.list
  it(`should list the first space`, function(done) {
    client.spaces.list({limit: 1, offse: 0})
    .then(response => {
      done()
    })
    .catch(error => {
      done(error)
    })
  })

  // test spaces.find
  it(`should find the space we just created`, function(done) {
    client.spaces.find(config.createSpaceParams.uri)
    .then(response => {
      // store ID of the space we created
      uuid = response.id
      done()
    })
    .catch(error => {
      done(error)
    })
  })

  // test spaces.resetPasscode
  it(`should reset the passcode on the space we just created`, function(done) {
    client.spaces.resetPasscode({uri: config.createSpaceParams.uri, passcode: '123456'})
    .then(response => {
      done()
    })
    .catch(error => {
      done(error)
    })
  })

  // test spaces.delete
  it(`should delete the space we just created`, function(done) {
    client.spaces.delete(uuid)
    .then(response => {
      done()
    })
    .catch(error => {
      done(error)
    })
  })
})
